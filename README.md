# Configuration Microservice Description

*Configuration Microservice* - stores the desired device configuration.

## Functional Description

- When adding a new device configuration to the catalog, the user specifies the `serial number`, `IP address`, `subnet mask` of the device configuration.  
- The `serial number` is unique. The application should not allow you to add multiple device configuration with the same serial number to the database.

---

## OAPI Description

| OAPI Function                                              | Headers                                 |                                          Body                                          | Description                                                    |
|------------------------------------------------------------|-----------------------------------------|:--------------------------------------------------------------------------------------:|----------------------------------------------------------------|
| `GET /api/service/configration`                            | - Content-Type: application/stream+json |                                            -                                           | Load a list of all device configurations from the directory    |
| `GET /api/service/configration/#{value}`                   | - Content-Type: application/json        |                                            -                                           | Load one device configuration based on serial number.          |
| `POST /api/service/configration`                           | - Content-Type: application/json        | { "ssn": "Serial Number", "ip": "IP address", "netmask": "Subnet" }                    | Add a new device configuration to the catalog                  |

---

## Device Configuration Description

The device in the catalog contains the following fields:

- `id` - identifier
- `ssn` - serial number
- `ip` - IP address
- `netmask` - subnetwork or subnet

---