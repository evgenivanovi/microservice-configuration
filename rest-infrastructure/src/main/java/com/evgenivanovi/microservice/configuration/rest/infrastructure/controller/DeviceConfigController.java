package com.evgenivanovi.microservice.configuration.rest.infrastructure.controller;

import com.evgenivanovi.microservice.configuration.rest.port.model.DeviceConfigDocument;
import com.evgenivanovi.microservice.configuration.rest.port.model.DeviceConfigPort;
import com.evgenivanovi.microservice.configuration.rest.port.model.DeviceItemConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class DeviceConfigController {

    static final String CONFIGURATION_PATH = "/api/service/configuration";
    static final String ID_PATH = "/{id}";

    private final DeviceConfigPort deviceConfigPort;

    @RequestMapping(
            method = RequestMethod.POST, path = CONFIGURATION_PATH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Mono<DeviceItemConfig> addDevice(@RequestBody final DeviceConfigDocument deviceConfigDocument) {
        return deviceConfigPort.addDevice(deviceConfigDocument);
    }

    @RequestMapping(
            method = RequestMethod.GET, path = CONFIGURATION_PATH,
            produces = MediaType.APPLICATION_STREAM_JSON_VALUE,
            consumes = MediaType.APPLICATION_STREAM_JSON_VALUE
    )
    public Flux<DeviceItemConfig> searchDevices() {
        return deviceConfigPort.searchAllDevices();
    }

    @RequestMapping(
            method = RequestMethod.GET, path = CONFIGURATION_PATH + ID_PATH,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public Mono<DeviceItemConfig> searchDevice(@PathVariable(name = "id") final String id) {
        return deviceConfigPort.searchDevice(id);
    }

}
