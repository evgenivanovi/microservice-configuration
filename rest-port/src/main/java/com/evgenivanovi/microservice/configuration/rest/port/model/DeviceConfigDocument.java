package com.evgenivanovi.microservice.configuration.rest.port.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeviceConfigDocument {

    private String ssn;
    private String ip;
    private String netmask;

}
