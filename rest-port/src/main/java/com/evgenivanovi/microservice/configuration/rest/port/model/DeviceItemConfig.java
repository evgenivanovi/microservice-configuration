package com.evgenivanovi.microservice.configuration.rest.port.model;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Wither;
import reactor.core.publisher.Mono;

@Getter
@Wither
@Builder
public class DeviceItemConfig {

    private String id;
    private String ssn;
    private String ip;
    private String netmask;

    public Mono<DeviceItemConfig> toMono() {
        return Mono.just(this);
    }

    public static Mono<DeviceItemConfig> emptyMono() {
        return DeviceItemConfig.empty().toMono();
    }

    public static DeviceItemConfig empty() {
        return DeviceItemConfig.builder().build();
    }

}
