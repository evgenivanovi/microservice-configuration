package com.evgenivanovi.microservice.configuration.rest.port.service;

import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfig;
import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigManager;
import com.evgenivanovi.microservice.configuration.rest.port.model.DeviceConfigDocument;
import com.evgenivanovi.microservice.configuration.rest.port.model.DeviceConfigPort;
import com.evgenivanovi.microservice.configuration.rest.port.model.DeviceItemConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
public class DeviceConfigPortService implements DeviceConfigPort {

    private final DeviceConfigManager deviceConfigManager;

    @Override
    public Flux<DeviceItemConfig> searchAllDevices() {
        log.info("Registry Port: Search All Devices");
        return deviceConfigManager.searchAllDevices()
                .map(DeviceConfigPortService::toDeviceItemConfig);
    }

    @Override
    public Mono<DeviceItemConfig> searchDevice(final String id) {
        log.info("Registry Port: Search Device By ID");
        return deviceConfigManager.searchDevice(id)
                .map(DeviceConfigPortService::toDeviceItemConfig);
    }

    @Override
    public Mono<DeviceItemConfig> addDevice(final DeviceConfigDocument deviceConfigDocument) {
        log.info("Registry Port: Add Device");
        return deviceConfigManager.addDevice(toDeviceConfig(deviceConfigDocument))
                .map(DeviceConfigPortService::toDeviceItemConfig);
    }

    private static DeviceConfig toDeviceConfig(final DeviceConfigDocument deviceConfigDocument) {
        return DeviceConfig.builder()
                .ssn(deviceConfigDocument.getSsn())
                .ip(deviceConfigDocument.getIp())
                .netmask(deviceConfigDocument.getNetmask())
                .build();
    }

    private static DeviceItemConfig toDeviceItemConfig(final DeviceConfig deviceConfig) {
        return DeviceItemConfig.builder()
                .id(deviceConfig.getId())
                .ssn(deviceConfig.getSsn())
                .ip(deviceConfig.getIp())
                .netmask(deviceConfig.getNetmask())
                .build();
    }

}
