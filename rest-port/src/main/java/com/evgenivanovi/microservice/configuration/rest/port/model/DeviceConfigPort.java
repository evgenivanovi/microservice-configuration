package com.evgenivanovi.microservice.configuration.rest.port.model;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DeviceConfigPort {

    Flux<DeviceItemConfig> searchAllDevices();

    Mono<DeviceItemConfig> searchDevice(String id);

    Mono<DeviceItemConfig> addDevice(DeviceConfigDocument deviceConfigDocument);

}
