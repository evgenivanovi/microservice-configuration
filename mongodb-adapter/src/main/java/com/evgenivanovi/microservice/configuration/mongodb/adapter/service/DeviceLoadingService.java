package com.evgenivanovi.microservice.configuration.mongodb.adapter.service;

import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfig;
import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigLoader;
import com.evgenivanovi.microservice.configuration.mongodb.adapter.model.DeviceDocument;
import com.evgenivanovi.microservice.configuration.mongodb.adapter.model.DeviceIndex;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class DeviceLoadingService implements DeviceConfigLoader {

    private final DeviceIndex deviceIndex;

    @Override
    public Flux<DeviceConfig> searchAllDevices() {
        return deviceIndex.searchDevices(new Query())
                .map(DeviceLoadingService::toDevice);
    }

    @Override
    public Mono<DeviceConfig> searchDevice(final String id) {
        final Criteria criteria = toIdCriteria(id);
        return deviceIndex.searchDevice(Query.query(criteria))
                .map(DeviceLoadingService::toDevice);
    }

    private static DeviceConfig toDevice(final DeviceDocument deviceDocument) {
        return DeviceConfig.builder()
                .id(deviceDocument.getId())
                .ssn(deviceDocument.getSsn())
                .ip(deviceDocument.getIp())
                .netmask(deviceDocument.getNetmask())
                .build();
    }

    private static Criteria toIdCriteria(final String id) {
        return Criteria
                .where("ssn").is(id);
    }

}
