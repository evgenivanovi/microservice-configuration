package com.evgenivanovi.microservice.configuration.mongodb.adapter.service;

import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfig;
import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigSaver;
import com.evgenivanovi.microservice.configuration.mongodb.adapter.model.DeviceDocument;
import com.evgenivanovi.microservice.configuration.mongodb.adapter.model.DeviceIndex;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class DeviceConfigSavingService implements DeviceConfigSaver {

    private final DeviceIndex deviceIndex;

    @Override
    public Mono<DeviceConfig> saveDevice(final DeviceConfig deviceInfo) {
        return deviceIndex.saveDevice(DeviceConfigSavingService.toDeviceDocument(deviceInfo))
                .map(DeviceConfigSavingService::toDeviceInfo);
    }

    private static DeviceConfig toDeviceInfo(final DeviceDocument deviceDocument) {
        return DeviceConfig.builder()
                .id(deviceDocument.getId())
                .ssn(deviceDocument.getSsn())
                .ip(deviceDocument.getIp())
                .netmask(deviceDocument.getNetmask())
                .build();
    }

    private static DeviceDocument toDeviceDocument(final DeviceConfig deviceInfo) {
        return DeviceDocument.builder()
                .id(deviceInfo.getId())
                .ssn(deviceInfo.getSsn())
                .ip(deviceInfo.getIp())
                .netmask(deviceInfo.getNetmask())
                .build();
    }

}
