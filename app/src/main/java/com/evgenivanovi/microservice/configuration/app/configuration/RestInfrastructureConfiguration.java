package com.evgenivanovi.microservice.configuration.app.configuration;

import com.evgenivanovi.microservice.configuration.rest.infrastructure.controller.ControllerErrorHandler;
import com.evgenivanovi.microservice.configuration.rest.infrastructure.controller.DeviceConfigController;
import com.evgenivanovi.microservice.configuration.rest.port.model.DeviceConfigPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestInfrastructureConfiguration {

    @Bean
    DeviceConfigController registryController(final DeviceConfigPort deviceConfigPort) {
        return new DeviceConfigController(deviceConfigPort);
    }

    @Bean
    ControllerErrorHandler controllerErrorHandler() {
        return new ControllerErrorHandler();
    }

}
