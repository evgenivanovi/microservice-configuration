package com.evgenivanovi.microservice.configuration.app.configuration;

import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigLoader;
import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigSaver;
import com.evgenivanovi.microservice.configuration.mongodb.adapter.model.DeviceIndex;
import com.evgenivanovi.microservice.configuration.mongodb.adapter.service.DeviceLoadingService;
import com.evgenivanovi.microservice.configuration.mongodb.adapter.service.DeviceConfigSavingService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MongoAdapterInfrastructureConfiguration {

    @Bean
    DeviceConfigLoader deviceLoader(final DeviceIndex deviceIndex) {
        return new DeviceLoadingService(deviceIndex);
    }

    @Bean
    DeviceConfigSaver deviceSaver(final DeviceIndex deviceIndex) {
        return new DeviceConfigSavingService(deviceIndex);
    }

}
