package com.evgenivanovi.microservice.configuration.app.configuration;

import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigManager;
import com.evgenivanovi.microservice.configuration.rest.port.model.DeviceConfigPort;
import com.evgenivanovi.microservice.configuration.rest.port.service.DeviceConfigPortService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestPortConfiguration {

    @Bean
    DeviceConfigPort registryPort(final DeviceConfigManager deviceConfigManager) {
        return new DeviceConfigPortService(deviceConfigManager);
    }

}
