package com.evgenivanovi.microservice.configuration.app.configuration;

import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigLoader;
import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigManager;
import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigSaver;
import com.evgenivanovi.microservice.configuration.domain.service.DeviceConfigManagerService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfiguration {

    @Bean
    DeviceConfigManager deviceManager(final DeviceConfigLoader deviceLoader, final DeviceConfigSaver deviceConfigSaver) {
        return new DeviceConfigManagerService(deviceLoader, deviceConfigSaver);
    }

}
