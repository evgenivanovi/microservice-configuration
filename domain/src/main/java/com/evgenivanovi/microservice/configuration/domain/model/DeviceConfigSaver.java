package com.evgenivanovi.microservice.configuration.domain.model;

import reactor.core.publisher.Mono;

public interface DeviceConfigSaver {

    Mono<DeviceConfig> saveDevice(DeviceConfig device);

}
