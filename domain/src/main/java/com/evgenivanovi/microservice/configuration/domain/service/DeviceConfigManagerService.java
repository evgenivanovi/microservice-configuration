package com.evgenivanovi.microservice.configuration.domain.service;

import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfig;
import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigLoader;
import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigManager;
import com.evgenivanovi.microservice.configuration.domain.model.DeviceConfigSaver;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class DeviceConfigManagerService implements DeviceConfigManager {

    private final DeviceConfigLoader deviceConfigLoader;
    private final DeviceConfigSaver deviceConfigSaver;

    @Override
    public Flux<DeviceConfig> searchAllDevices() {
        return deviceConfigLoader.searchAllDevices();
    }

    @Override
    public Mono<DeviceConfig> searchDevice(final String id) {
        return deviceConfigLoader.searchDevice(id);
    }

    @Override
    public Mono<DeviceConfig> addDevice(final DeviceConfig deviceConfig) {
        return deviceConfigSaver.saveDevice(deviceConfig);
    }

}
