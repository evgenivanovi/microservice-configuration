package com.evgenivanovi.microservice.configuration.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Wither;

@Getter
@Wither
@Builder
public class DeviceConfig {

    private String id;
    private String ssn;
    private String ip;
    private String netmask;

}
