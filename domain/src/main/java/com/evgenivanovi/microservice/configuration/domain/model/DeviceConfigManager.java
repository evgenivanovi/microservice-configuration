package com.evgenivanovi.microservice.configuration.domain.model;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface DeviceConfigManager {

    Flux<DeviceConfig> searchAllDevices();

    Mono<DeviceConfig> searchDevice(String id);

    Mono<DeviceConfig> addDevice(DeviceConfig device);

}
